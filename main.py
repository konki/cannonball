import numpy as np
import pygame as pg

from cannon import Cannonball


def rotate(window, surface, angle):
    rotated_surface = pg.transform.rotozoom(surface, angle, 1)

    _, surface_height = surface.get_size()
    _, win_height = window.get_size()

    rotated_surface_rect = rotated_surface.get_rect(
        bottomleft=(
            surface_height * (1 - np.sin(angle * 2 * np.pi / 360)),
            win_height - surface_height,
        )
    )
    return rotated_surface, rotated_surface_rect


def new_cannonball(dict, max_balls, posx, posy, vx, vy):
    for key in dict:
        ball = dict[key]
        if ball == None:
            new_ball = Cannonball(key, posx, posy, vx, vy)
            dict[key] = new_ball
            print(f"New ball created with id: {key}")
            break


def remove_cannonball(dict, key):
    dict[key] = None


def run_game(window):
    running = True
    timer = pg.time.Clock()

    pg.font.init()
    font = pg.font.SysFont("Fire Sans Book", 60)

    win_width, win_height = window.get_size()

    cannon_width, cannon_height = 200, 100
    cannon = pg.Surface((cannon_width, cannon_height), pg.SRCALPHA)
    cannon_rect = cannon.get_rect(
        bottomleft=(cannon_height, win_height - cannon_height)
    )
    cannon.fill((69, 69, 69))

    theta = 0
    v = 70

    fps = 120
    dt = 1 / fps

    rotated_cannon, rotated_cannon_rect = cannon, cannon_rect

    rot_ang = 5
    dv = 5

    max_balls = 10
    balls_dict = dict.fromkeys(range(1, max_balls + 1))

    target_min_height = cannon_height / 2
    target_thickness = 20
    target_hit = True

    points = 0

    while running:
        g = -9.81 * (win_width / cannon_width)
        for event in pg.event.get():
            if event.type == pg.KEYDOWN:
                if (event.key == pg.K_UP or event.key == pg.K_w) and theta <= (
                    90 - rot_ang
                ):
                    theta += rot_ang
                elif (event.key == pg.K_DOWN or event.key == pg.K_s) and theta > 0:
                    theta -= rot_ang
                elif (event.key == pg.K_RIGHT or event.key == pg.K_d) and v <= 1000:
                    v += dv
                elif (event.key == pg.K_LEFT or event.key == pg.K_a) and v > 0:
                    v -= dv
                elif event.key == pg.K_SPACE:
                    posx = cannon_height + (
                        cannon_width * np.cos(theta * 2 * np.pi / 360)
                        - cannon_height / 2 * np.sin(theta * 2 * np.pi / 360)
                    )
                    posy = (
                        win_height
                        - cannon_height
                        - (
                            cannon_width * np.sin(theta * 2 * np.pi / 360)
                            + cannon_height / 2 * np.cos(theta * 2 * np.pi / 360)
                        )
                    )
                    vx = (
                        (v * np.cos(theta * 2 * np.pi / 360)) * win_width / cannon_width
                    )
                    vy = (
                        (v * np.sin(theta * 2 * np.pi / 360)) * win_width / cannon_width
                    )
                    new_cannonball(balls_dict, max_balls, posx, posy, vx, vy)
            elif event.type == pg.QUIT:
                running = False

        win_width, win_height = window.get_size()

        window.fill((255, 255, 255))

        if target_hit == True:
            target_height = target_min_height + (cannon_height * np.random.random())
            target_center_y_offset_scale = np.random.random()
            target_hit = False

        target_center_x = win_width - target_thickness / 2
        target_center_y = (
            target_height / 2
            + (win_height - target_height) * target_center_y_offset_scale
        )

        target = pg.Surface((target_thickness, target_height))
        target_rect = target.get_rect(center=(target_center_x, target_center_y))
        target.fill((255, 0, 0))

        window.blit(target, target_rect)

        for key in balls_dict:
            ball = balls_dict[key]
            if ball != None:
                ball.update(dt, 0, g)
                ball_circle = pg.draw.circle(
                    window, (50, 50, 50), (ball.posx, ball.posy), cannon_height / 4
                )
                if ball_circle.colliderect(
                    target_rect
                ):  # (win_width - target_thickness < ball.posx < win_width) and (target_center_y - target_height / 2 < ball.posy < target_center_y + target_height / 2):
                    print(f"Ball {key} hit the target")
                    target_hit = True
                    points += 1
                    remove_cannonball(balls_dict, key)
                elif (
                    ball.posx <= 0
                    or ball.posx >= win_width
                    or ball.posy <= 0
                    or ball.posy >= win_height
                ):
                    print(f"Ball {ball.id} hit the edges.")
                    remove_cannonball(balls_dict, key)

        rotated_cannon, rotated_cannon_rect = rotate(window, cannon, theta)

        window.blit(rotated_cannon, rotated_cannon_rect)
        pg.draw.circle(
            window,
            (105, 29, 15),
            (cannon_height, win_height - cannon_height),
            cannon_height / 2,
        )

        textbox_1 = font.render(f"{v = } m/s; θ = {theta}°", True, (0, 0, 0))
        textbox_1_rect = textbox_1.get_rect(topright=(win_width - 5, 5))
        window.blit(textbox_1, textbox_1_rect)

        textbox_2 = font.render(f"Points: {points}", True, (0, 0, 0))
        textbox_2_rect = textbox_2.get_rect(topright=(win_width - 5, 5 + 45))
        window.blit(textbox_2, textbox_2_rect)
        # Update our window
        pg.display.flip()
        timer.tick(fps)


def main():
    pg.init()
    window = pg.display.set_mode((1000, 1000), pg.RESIZABLE)

    run_game(window)


if __name__ == "__main__":
    main()
