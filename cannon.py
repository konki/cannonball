class Cannonball:
    """Class to track cannonballs"""

    def __init__(self, id, posx, posy, vx, vy):
        """Initialize the cannonball"""
        self.id = id
        self.posx = posx
        self.posy = posy
        self.vx = vx
        self.vy = vy

    def update(self, dt, ax, ay):
        """Update cannonball velocity and position based on acceleration"""
        self.vx += ax * dt
        self.vy += ay * dt

        self.posx += self.vx * dt
        self.posy -= self.vy * dt
